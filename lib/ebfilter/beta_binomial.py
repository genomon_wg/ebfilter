#! /usr/bin/env python

import math
import numpy
import scipy.optimize
import scipy.special

def beta_binomial_density(params, n, k):

    alpha = params[0]
    beta = params[1]

    tempD = math.lgamma(n + 1) - math.lgamma(k + 1) - math.lgamma(n - k + 1)
    tempD = tempD - math.lgamma(n + alpha + beta) + math.lgamma(k + alpha) + math.lgamma(n - k + beta)
    tempD = tempD + math.lgamma(alpha + beta) - math.lgamma(alpha) - math.lgamma(beta) 

    return math.exp(tempD)

def beta_binom_pvalue(params, n, k):

    tempPV = 0
    for kk in range(k, n + 1):

        currentValue = beta_binomial_density(params, n, kk)
        tempPV = tempPV + currentValue


    return tempPV


def beta_binomial_loglikelihood(params, Ns, Ks):

    """Calculating log-likelihood of beta-binomial distribution

    Args:
        params (List[float]): the parameter of beta distribution ([alpha, beta])
    
        As (numpy.array([int])): the counts for success
        
        Bs (numpy.array([int])): the counts of trials

    """

    alpha = params[0]    
    beta = params[1]

    ML = 0
    ML = ML - sum(scipy.special.gammaln(Ns + alpha + beta))
    ML = ML + sum(scipy.special.gammaln(Ks + alpha))
    ML = ML + sum(scipy.special.gammaln(Ns - Ks + beta))

    ML = ML + len(Ns) * (math.lgamma(alpha + beta) - math.lgamma(alpha) - math.lgamma(beta))


    # Here, we set the penalty term of alpha and beta (0.5 is slightly arbitray...)
    ML = ML - 0.5 * math.log(alpha + beta)
    return -ML

 
def beta_binomial_loglikelihood_prime(params, Ns, Ks):

    alpha = params[0]
    beta = params[1]

    ML_a, ML_b = 0, 0

    tmp =  sum(scipy.special.digamma(Ns + alpha + beta))
    ML_a = ML_a - tmp
    ML_b = ML_b - tmp
    ML_a = ML_a + sum(scipy.special.digamma(Ks + alpha))
    ML_b = ML_b + sum(scipy.special.digamma(Ns - Ks + beta))
    tmp = scipy.special.digamma(alpha + beta)
    ML_a = ML_a + len(Ns) * (tmp - scipy.special.digamma(alpha))
    ML_b = ML_b + len(Ns) * (tmp - scipy.special.digamma(beta))

    ML_a = ML_a - 0.5 / (alpha + beta)
    ML_b = ML_b - 0.5 / (alpha + beta)

    return numpy.array([-ML_a, -ML_b])


def fit_beta_binomial(As, Bs):

    """Obtaining maximum likelihood estimator of beta-binomial distribution

    Args:
        As (numpy.array([int])): the counts for success
        
        Bs (numpy.array([int])): the counts of trials

    """

    result = scipy.optimize.fmin_l_bfgs_b(beta_binomial_loglikelihood,
                                          [20, 20],
                                          fprime = beta_binomial_loglikelihood_prime,
                                          args = (As, Bs),
                                          bounds = [(0.1, 10000000), (1, 10000000)])

    return result[0]




