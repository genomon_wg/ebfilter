#! /usr/bin/env python

import process_vcf
import process_anno
import get_eb_score
import sys, os, subprocess, math, re, multiprocessing 
import functools
import vcf, pysam, numpy

def EBFilter_worker_vcf(targetMutationFile, targetBamPath, controlBamPathList, outputPath, mapping_qual_thres, base_qual_thres):

    controlFileNum = sum(1 for line in open(controlBamPathList, 'r'))

    ##########
    # generate pileup files
    process_vcf.vcf2pileup(targetMutationFile, outputPath + '.target.pileup', targetBamPath, mapping_qual_thres, base_qual_thres, False)
    process_vcf.vcf2pileup(targetMutationFile, outputPath + '.control.pileup', controlBamPathList, mapping_qual_thres, base_qual_thres, True)
    ##########

    ##########
    # load pileup files
    pos2pileup_target = {}
    pos2pileup_control = {}

    hIN = open(outputPath + '.target.pileup')
    for line in hIN:
        F = line.rstrip('\n').split('\t')
        pos2pileup_target[F[0] + '\t' + F[1]] = '\t'.join(F[3:])
    hIN.close()

    hIN = open(outputPath + '.control.pileup')
    for line in hIN:
        F = line.rstrip('\n').split('\t')
        pos2pileup_control[F[0] + '\t' + F[1]] = '\t'.join(F[3:])
    hIN.close()
    ##########

    vcf_reader = vcf.Reader(open(targetMutationFile, 'r'))
    vcf_reader.infos['EB'] = vcf.parser._Info('EB', 1, 'Float', "EBCall Score", "EBCall", "ver0.1.0")
    vcf_writer =vcf.Writer(open(outputPath, 'w'), vcf_reader)


    for vcf_record in vcf_reader:
        current_pos = str(vcf_record.CHROM) + '\t' + str(vcf_record.POS) 
        F_target = pos2pileup_target[current_pos].split('\t') if current_pos in pos2pileup_target else []
        F_control = pos2pileup_control[current_pos].split('\t') if current_pos in pos2pileup_control else []

        current_ref = str(vcf_record.REF)
        current_alt = str(vcf_record.ALT[0])
        var = ""
        if len(current_ref) == 1 and len(current_alt) == 1:
            var = current_alt
        else:
            if len(current_ref) == 1:
                var = "+" + current_alt[1:]
            elif len(current_alt) == 1:
                var = "-" + current_ref[1:]

        EB_score = "." # if the variant is complex, we ignore that
        if not var == "":
            EB_score = get_eb_score.get_eb_score(var, F_target, F_control, base_qual_thres, controlFileNum)

        # add the score and write the vcf record
        vcf_record.INFO['EB'] = EB_score
        vcf_writer.write_record(vcf_record)

    vcf_writer.close()


    # delete intermediate files
    subprocess.call(["rm", outputPath + '.target.pileup'])
    subprocess.call(["rm", outputPath + '.control.pileup'])


def EBFilter_worker_anno(targetMutationFile, targetBamPath, controlBamPathList, outputPath, mapping_qual_thres, base_qual_thres):

    controlFileNum = sum(1 for line in open(controlBamPathList, 'r'))

    ##########
    # generate pileup files
    process_anno.anno2pileup(targetMutationFile, outputPath + '.target.pileup', targetBamPath, mapping_qual_thres, base_qual_thres, False)
    process_anno.anno2pileup(targetMutationFile, outputPath + '.control.pileup', controlBamPathList, mapping_qual_thres, base_qual_thres, True)
    ##########

    ##########
    # load pileup files
    pos2pileup_target = {}
    pos2pileup_control = {}

    hIN = open(outputPath + '.target.pileup')
    for line in hIN:
        F = line.rstrip('\n').split('\t')
        pos2pileup_target[F[0] + '\t' + F[1]] = '\t'.join(F[3:])
    hIN.close()

    hIN = open(outputPath + '.control.pileup')
    for line in hIN:
        F = line.rstrip('\n').split('\t')
        pos2pileup_control[F[0] + '\t' + F[1]] = '\t'.join(F[3:])
    hIN.close()
    ##########

    hIN = open(targetMutationFile, 'r')
    hOUT = open(outputPath, 'w')


    for line in hIN:

        F = line.rstrip('\n').split('\t')
        chr, pos, pos2, ref, alt = F[0], F[1], F[2], F[3], F[4]
        if alt == "-": pos = str(int(pos) - 1)

        F_target = pos2pileup_target[chr + '\t' + pos].split('\t') if chr + '\t' + pos in pos2pileup_target else []
        F_control = pos2pileup_control[chr + '\t' + pos].split('\t') if chr + '\t' + pos in pos2pileup_control else [] 

        var = ""
        if ref != "-" and alt != "-":
            var = alt
        else:
            if ref == "-":
                var = "+" + alt
            elif alt == "-":
                var = "-" + ref

        EB_score = "." # if the variant is complex, we ignore that
        if not var == "":
            EB_score = get_eb_score.get_eb_score(var, F_target, F_control, base_qual_thres, controlFileNum)

        # add the score and write the vcf record
        print >> hOUT, '\t'.join(F + [str(EB_score)])

    hIN.close()
    hOUT.close()


    # delete intermediate files
    subprocess.call(["rm", outputPath + '.target.pileup'])
    subprocess.call(["rm", outputPath + '.control.pileup'])


def EBFilter_worker_anno_1(targetBamPath, controlBamPathList, outputPath, mapping_qual_thres, base_qual_thres, suffix, pool_arg):

    i, line = pool_arg
    outputPath = outputPath + suffix % i

    controlFileNum = sum(1 for line in open(controlBamPathList, 'r'))

    F = line.rstrip('\n').split('\t')
    chr, pos, pos2, ref, alt = F[0], F[1], F[2], F[3], F[4]
    if alt == "-": pos = str(int(pos) - 1)

    F_target  = process_anno.anno2pileup_1(chr, pos, targetBamPath, mapping_qual_thres, base_qual_thres, False)
    F_control = process_anno.anno2pileup_1(chr, pos, controlBamPathList, mapping_qual_thres, base_qual_thres, True)

    var = ""
    if ref != "-" and alt != "-":
        var = alt
    else:
        if ref == "-":
            var = "+" + alt
        elif alt == "-":
            var = "-" + ref

    EB_score = "." # if the variant is complex, we ignore that
    if not var == "":
        EB_score = get_eb_score.get_eb_score(var, F_target, F_control, base_qual_thres, controlFileNum)

    # add the score and write the vcf record
    hOUT = open(outputPath, 'w')
    print >> hOUT, '\t'.join(F + [str(EB_score)])
    hOUT.close()



def main(args):

    # should add validity check for arguments
    targetMutationFile = args.targetMutationFile
    targetBamPath = args.targetBamPath
    controlBamPathList = args.controlBamPathList
    outputPath = args.outputPath

    mapping_qual_thres = args.q
    base_qual_thres = args.Q
    thread_num = args.t
    chunk_size = args.chunk_size
    is_anno = True if args.f == 'anno' else False


    # file existence check
    if not os.path.exists(targetMutationFile):
        print >> sys.stderr, "No target mutation file: " + targetMutationFile
        sys.exit(1)

    if not os.path.exists(targetBamPath):
        print >> sys.stderr, "No target bam file: " + targetBamPath
        sys.exit(1)

    if not os.path.exists(targetBamPath + ".bai") and not os.path.exists(re.sub(r'bam$', "bai", targetBamPath)):
        print >> sys.stderr, "No index for target bam file: " + targetBamPath
        sys.exit(1)


    if not os.path.exists(controlBamPathList):
        print >> sys.stderr, "No control list file: " + controlBamPathList 
        sys.exit(1)

    with open(controlBamPathList) as hIN:
        for file in hIN:
            file = file.rstrip()
            if not os.path.exists(file):
                print >> sys.stderr, "No control bam file: " + file 
                sys.exit(1)

            if not os.path.exists(file + ".bai") and not os.path.exists(re.sub(r'bam$', "bai", file)):
                print >> sys.stderr, "No index control bam file: " + file 
                sys.exit(1)

     
    if thread_num == 1:
        # non multi-threading mode
        if is_anno == True:
            EBFilter_worker_anno(targetMutationFile, targetBamPath, controlBamPathList, outputPath, mapping_qual_thres, base_qual_thres)
        else: 
            EBFilter_worker_vcf(targetMutationFile, targetBamPath, controlBamPathList, outputPath, mapping_qual_thres, base_qual_thres)
    else:
        # multi-threading mode
        ##########

        if is_anno == True:
            if chunk_size > 0:
                # multiprocessing.Pool
                num_lines = int(subprocess.check_output(['wc', '-l', targetMutationFile]).split(' ')[0])
                if num_lines > 0:
                    n = int(math.log10(num_lines)) + 1
                else:
                    n = 1
                suffix = '.%0' + '%d' % (n + 1) + 'd'

                hIN = open(targetMutationFile, 'r')
                pool = multiprocessing.Pool(thread_num)

                pool.map(functools.partial(EBFilter_worker_anno_1,
                                           targetBamPath, controlBamPathList, outputPath,
                                           mapping_qual_thres, base_qual_thres, suffix),
                         enumerate(hIN), chunk_size)
                
                pool.close()
                hIN.close()

                # merge result files
                hOUT = open(outputPath, 'w')
                for i in range(num_lines):
                    in_file = outputPath + suffix % i
                    hIN = open(in_file)
                    hOUT.write(hIN.read())
                    hIN.close()
                    os.unlink(in_file)
                hOUT.close()

            else:
                # partition anno files
                thread_num = process_anno.partition_anno(targetMutationFile, outputPath + ".tmp.input.anno.", thread_num)
    
                jobs = []
                for i in range(thread_num):
                    process = multiprocessing.Process(target = EBFilter_worker_anno, args = \
                        (outputPath + ".tmp.input.anno." + str(i), targetBamPath, controlBamPathList, outputPath + "." + str(i), mapping_qual_thres, base_qual_thres))
                    jobs.append(process)
                    process.start()
            
                # wait all the jobs to be done
                for i in range(thread_num):
                    jobs[i].join()
            
                # merge the individual results
                process_anno.merge_anno(outputPath + ".", outputPath, thread_num)
            
                # delete intermediate files
                for i in range(thread_num):
                    subprocess.call(["rm", outputPath + ".tmp.input.anno." + str(i)])
                    subprocess.call(["rm", outputPath + "." + str(i)])

        else:
            # partition vcf files
            thread_num = process_vcf.partition_vcf(targetMutationFile, outputPath + ".tmp.input.vcf.", thread_num)

            jobs = []
            for i in range(thread_num):
                process = multiprocessing.Process(target = EBFilter_worker_vcf, args = \
                    (outputPath + ".tmp.input.vcf." + str(i), targetBamPath, controlBamPathList, outputPath + "." + str(i), mapping_qual_thres, base_qual_thres))
                jobs.append(process)
                process.start()

            # wait all the jobs to be done
            for i in range(thread_num):
                jobs[i].join()

            # merge the individual results
            process_vcf.merge_vcf(outputPath + ".", outputPath, thread_num)

            # delete intermediate files
            for i in range(thread_num):
                subprocess.call(["rm", outputPath + ".tmp.input.vcf." + str(i)])
                subprocess.call(["rm", outputPath + "." + str(i)])



